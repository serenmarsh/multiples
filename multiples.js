// Seren
// 15/03
// 10010681

// declaring variables
let startNum = 2;
let endNum = 8;

// Heading
console.log("N    10*N    100*N    1000*N");
console.log("-    ----    -----    ------");

//for loop
console.log("--> for loop <--");
for (var i = startNum; i <= endNum; i++) {
    console.log(`${i}     ${i * 10}      ${i * 100}      ${i * 1000}` );
}

//while loop
console.log("--> while loop <--");
i = startNum;
while (i <= endNum) {
    console.log(`${i}    ${i * 10}       ${i * 100}      ${i * 1000}` );
    i++
}

//do while loop
console.log("--> do while loop <--")
i = startNum;
do {
    console.log(`${i}    ${i * 10}      ${i * 100}       ${i * 1000}` );
    i++
}
while (i <= endNum)